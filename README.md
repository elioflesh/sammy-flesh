![](https://elioway.gitlab.io/elioflesh/sammy-flesh/elio-sammy-flesh-logo.png)

> Sammy in the flesh, **the elioWay**

# sammy-flesh

![experimental](https://elioway.gitlab.io/eliosin/icon/devops/experimental/favicon.ico "experimental")

A SinglePageApp, made with **SammyJs**, consuming the **mongoose-bones** REST API, **the elioWay**.

## Requirements

- [mongo server](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/)

## Seeing is Believing

```shell
git clone https://gitlab.com/elioflesh/flesh.git
cd flesh
npm i
gulp
```

DevDependies:

```
// package.json
"auto-reload-brunch": "^2",
"babel-brunch": "~6.0",
"babel-preset-latest": "^6",
"brunch": "^2",
"clean-css-brunch": "^2",
"uglify-js-brunch": "^2"
```

## License

[MIT](LICENSE) [Tim Bushell](mailto:theElioWay@gmail.com)

[elioWay](https://gitlab.com/elioway/elio/blob/master/README.html)

![](https://elioway.gitlab.io/elioflesh/sammy-flesh/apple-touch-icon.png)
