{{#thing}}
<h2>
  {{name}}
</h2>

<nav>
  <a href="#/update/Thing/{{_id}}/" alt="Edit {{name}}"><button>Update</button></a>
  <a href="#/delete/Thing/{{_id}}/" alt="Delete {{name}}"><button>Delete</button></a>
</nav>

<dl>
  {{#_fields}}
  <dt>{{key}}</dt><dd>{{val}}</dd>
  {{/_fields}}
</dl>

{{/thing}}

{{#^thing}}
<p>
  This thing was not found.
</p>
{{/^thing}}
