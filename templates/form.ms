{{#thing}}
<h3>{{#^_id}}New Thing{{/^_id}} {{#_id}}Edit{{/_id}} {{_id}}</h3>

<form action="#/{{url}}/Thing/{{update}}/" method="post">
  <fieldset>
    {{#^new}}
    <legend>
      {{disambiguatingDescription}}
    </legend>
    {{/^new}}

    {{#_fields}}
    <label for="{{key}}">{{key}}</label>
    <input autofocus
      name="{{key}}"
      type="text"
      placeholder="{{key}}"
      value="{{val}}"
      >
    {{/_fields}}

    {{#_id}}
    <input required name="_id" type="hidden" value="{{_id}}">
    {{/_id}}

  </fieldset>

  <button type="submit">Submit</button>
  <button>Cancel</button>
</form>
{{/thing}}

{{#^thing}}
<p>
  This thing was not found.

  <a href="#/get/Thing"><button>Cancel</button></a>
</p>
{{/^thing}}
