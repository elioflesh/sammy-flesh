<h1>{{title}}</h1>

{{#home}}

<nav>
  <a href="/"><button>Exit</button></a>
</nav>

<p>
  We're using SammyJS to build a no-frills, bare-bones, SinglePageApp in this
  first iteration of <b>flesh</b>. We want to keep things really, really simple for
  version 1 of all things done **the elioWay**. Need more info? Click "About"!
</p>
<p>
  Well done, you! for continuing to do things **the elioWay**. You know you made the
  right choice.
</p>
{{/home}}

{{#about}}
<h2>
  "flesh" is simple.
</h2>
<p>
  HTML was supposed to be simple. "sin" is simple. So "god" is simple. "spider"
  is simple. "bones" is simple.
</p>
<h3>
  The SammyJS way
</h3>

<pre>
&lt;script&gt;

  (function($) {

    var app = Sammy('main', function() {

      this.use('Mustache', 'ms')

      this.get('#/', function() {
        this.title = 'Welcome back home to Flesh'
        this.home = true
        this.partial('flesh/home.ms')
      })
    })

    app.run('#/')

  })(jQuery)

&lt;/script&gt;
</pre>
<ul>
  <li>In "flesh" (as with "eliosin" themes), you start at the index.html page.</li>
  <li>Add any fixed content, like banners or footers. <ul><li>You can also Sammy this if you want, but this is the simplest way.</li></ul></li>
  <li>Add a "main" block to the index.html page, e.g.:
  </li>
</ul>
<pre>
 &lt;main&gt;&lt;/main&gt;</pre>
<ul>
  <li>Add the main script shown to the index.html page <ul><li>Notice how it targets the same block "main"</li></ul></li>
  <li>Include the routes you want your app to have, e.g.:</li>
</ul>
<pre>
&lt;script src="routes/Home.js"&gt;&lt;/script&gt;
&lt;script src="routes/Thing.get.js"&gt;&lt;/script&gt;
&lt;script src="routes/Thing.delete.js"&gt;&lt;/script&gt;</pre>
<ul>
  <li>Then type "gulp" into your command line.</li>
</ul>
{{/about}}

{{#contact}}
<p>
  No aimless hellos, please. You can contact me about some <i>thing</i> using
  the email address below.
</p>
<p>
  Email: <a href="mailto:theElioWay@gmail.com">theElioWay@gmail.com</a>
</p>
{{/contact}}
