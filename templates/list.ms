<h2>
  Things List
</h2>

<nav>
  <a href="#/new/Thing/{{god}}/"><button>New</button></a>
</nav>

<table>
  <thead>
    <tr>
      <th>
        List
      </th>
      <th>
        &nbsp;
      </th>
      <th>
        &nbsp;
      </th>
    </tr>
  </thead>
  <tbody>
    {{#things}}
      <tr>
        <td>
          {{#_fields}}
            {{#val}}
              <a href="#/engage/Thing/{{_id}}/" alt="View {{View}}">{{val}}</a><br>
            {{/val}}
          {{/_fields}}
        </td>
        <td>
          <a href="#/update/Thing/{{_id}}/" alt="Edit {{name}}"><button>Update</button>
        </td>
        <td>
        <a href="#/delete/Thing/{{_id}}/" alt="Delete {{name}}"><button>Delete</button></a>
        </td>
      </tr>
    {{/things}}
  </tbody>
</table>
