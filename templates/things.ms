<h2>
  Things List
</h2>

<nav>
  <a href="#/new/Thing/{{god}}/"><button>New</button></a>
</nav>

{{#things}}
  {{#_fields}}
  <dl>
    {{#val}}
  <dt>{{key}}</dt><dd>{{val}}</dd>
    {{/val}}

    <a href="#/get/Thing/{{val._id}}" alt="View {{View}}"><button>View</button></a>
    <a href="#/update/Thing/{{val._id}}" alt="Edit {{val.name}}"><button>Update</button></a>
    <a href="#/delete/Thing/{{val._id}}" alt="Delete {{val.name}}"><button>Delete</button></a>
  </dl>
  {{/_fields}}
{{/things}}
