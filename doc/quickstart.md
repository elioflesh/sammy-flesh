# Quickstart sammy-flesh

- [sammy-flesh Prerequisites](/elioflesh/sammy-flesh/prerequisites.html)
- [Installing sammy-flesh](/elioflesh/sammy-flesh/installing.html)

## Nutshell

- Signup a new Thing

  ```
  curl -X POST http://localhost:5000/auth/MedicalCondition/signup -d name="MedicalCondition1" -d username="medicalcondition1" -d password="letmein"
  ```

- Edit the \$ENGAGED id in the `index.html` file.

- `gulp`

## Signup a new Thing

In **the elioWay**, everything is a Thing. We will let people signup many Things. This App will be hardcoded to one of those Thing.

First you need to Signup a new Thing, running the command:

```
curl -X POST http://localhost:5000/auth/Thing/signup -d name="Thing100" -d username="thing100" -d password="letmein"
```

Note the `_id` field in the response.

## The App is the Thing

In **the elioWay**, everything is a Thing. **sammy-flesh** is an SPA designed to engage with any 1 Thing, for instance _Thing100_ which we just signed up.

Open `index.html` and edit the \$ENGAGED id changing it to ID belonging to _Thing100_.

```
<a href="#/engage/Thing/5f4b845241f12f5b1a8524a0/"
  ><img
    src="node_modules/@elioway/icon/engage/favinverted.ico"
    title="Engage App"
/></a>
<a href="#/list/Thing/5f4b845241f12f5b1a8524a0/"
  ><img
    src="node_modules/@elioway/icon/list/favinverted.ico"
    title="App List"
/></a>
```

_Thing100_ is the the app.

You can run **sammy-flesh** by typing `gulp` and start adding new Things to _Thing100_.
