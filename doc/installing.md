# Installing sammy-flesh

- [Prerequisites](/elioflesh/sammy-flesh/prerequisites.html)

## Installing

```shell
git clone https://gitlab.com/elioflesh/sammy-flesh.git flesh-myapp
cd flesh-myapp
npm i
gulp
```

## Bundling

```shell
npm install -g brunch
```

```javascript
// brunch-config.js
// See http://brunch.io for documentation.
exports.files = {
  javascripts: {
    joinTo: {
      "vendor.js": /^(?!app)/, // Files that are not in `app` dir.
      "app.js": /^app/
    }
  },
  stylesheets: { joinTo: "app.css" }
}

exports.plugins = {
  babel: { presets: ["latest"] }
}
```
