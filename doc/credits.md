# sammy-flesh Credits

## Core! Ta Very Much

- [sammyjs](http://www.sammyjs.org)
- [nock](https://github.com/nock/nock)
- [nock tutorial:John Kariuki](https://scotch.io/tutorials/nodejs-tests-mocking-http-requests)
- [trigonometry-in-sass:Daniel Perez Alvarez](https://www.unindented.org/blog/trigonometry-in-sass/)

## These were useful

- [devdays:sammy-mustache](https://devdays.com/2014/05/25/single-page-apps-deep-dive-into-loading-templates-using-sammy-mustache-requirejs/)

## Apps to build

- <https://mithril.js.org/simple-application.html>
- <https://backbonejs.org/#Model-View-separation>
- <https://vuejs.org>

## Artwork

- [microphone](https://publicdomainvectors.org/en/free-clipart/Silhouette-vector-image-of-microphone-icon/13551.html)
- [The_Rat_Pack](https://commons.wikimedia.org/wiki/File:The_Rat_Pack_with_Jack_Entratter_at_the_Sands_1960.jpg)
