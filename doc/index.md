<aside>
  <dl>
  <dd>With feast and musick all the tents resound.</dd>
</dl>
</aside>

A SinglePageApp, made with **SammyJs**, consuming the **mongoose-bones** REST API, **the elioWay**.

# Requires

- [eliothing/thing](/eliothing/thing)

# Works Well With

- [eliobones/mongoose-bones](/eliobones/mongoose-bones)
