;(function($) {
  APP.around(function(callback) {
    var context = this
    // console.log({ boneySchema: "context" }, context)
    this.load(`${window.fleshConfig.bones}/schema/Thing/`, {
      json: true
    })
      .then(function(schema) {
        // console.log({ boneySchema: "schema" }, schema)
        context.schema = schema
      })
      .then(callback)
  })

  APP.get("#/engage/Thing/:id/", function(context) {
    var self = this
    // console.log({ boneyEngage: "context" }, context)
    // console.log({ boneyEngage: "params" }, self.params)
    this.load(`${window.fleshConfig.bones}/Thing/${self.params.id}/`, {
      json: true
    }).then(function(thing) {
      // console.log({ boneyEngage: "thing" }, thing)
      thing = context.fleshy(thing, context.schema)
      context.app.swap("")
      context
        .render(`${window.fleshConfig.templates}engage.ms`, {
          thing: thing
        })
        .appendTo(context.$element())
    })
  })

  APP.get("#/list/Thing/:id/", function(context) {
    var self = this
    // console.log({ boneyList: "context" }, context)
    // console.log({ boneyList: "params" }, self.params)
    this.load(`${window.fleshConfig.bones}/Thing/${self.params.id}/list/`, {
      json: true
    }).then(function(things) {
      // console.log({ boneyList: "things" }, things)
      things.forEach(thing => {
        thing = context.fleshy(thing, context.schema)
      })
      context.app.swap("")
      context
        .render(`${window.fleshConfig.templates}list.ms`, {
          god: self.params.id,
          things: things,
          schema: Object.keys(context.schema).filter(
            thing => !window.fleshConfig.metaFields.includes(thing)
          )
        })
        .appendTo(context.$element())
    })
  })

  APP.get("#/new/Thing/:id/", function(context) {
    var self = this
    // console.log({ boneyNew: "context" }, context)
    // console.log({ boneyNew: "params" }, self.params)
    thing = context.fleshy({}, context.schema)
    // console.log({ boneyNew: "thing" }, thing)
    context.app.swap("")
    context
      .render(`${window.fleshConfig.templates}form.ms`, {
        update: self.params.id,
        thing: thing,
        url: "create"
      })
      .appendTo(context.$element())
  })

  APP.get("#/update/Thing/:id/", function(context) {
    var self = this
    // console.log({ boneyUpdate: "context" }, context)
    // console.log({ boneyUpdate: "params" }, self.params)
    thing = context.fleshy({}, context.schema)
    try {
      this.load(`${window.fleshConfig.bones}/Thing/${self.params.id}/`, {
        json: true
      }).then(function(thing) {
        // console.log({ boneyUpdate: "thing" }, thing)
        thing = context.fleshy(thing, context.schema)
        context.app.swap("")
        context
          .render(`${window.fleshConfig.templates}form.ms`, {
            update: self.params.id,
            thing: thing,
            url: "patch"
          })
          .appendTo(context.$element())
      })
    } catch (err) {
      console.log({ boneyUpdate: "err" }, err)
    }
  })
})(jQuery)
