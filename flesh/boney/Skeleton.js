var Skeleton = function(app) {
  this.helpers({
    /**
     * `fleshy` expands the dictionary of this record with schema info.
     *
     * @param {json} bonesFormattedThing From the API, e.g.:
     *        {
     *          "field1": "val1",
     *          "field2": "val2",
     *          ...,
     *         }
     * @returns {json} bonesFormattedThing expanded to:
     *        {
     *          "field1": "val1",
     *          "field2": "val2",
     *          ...,
     *          "_fields": [displayFields with schema info],
     *          "_meta": [metaFields with schema info]
     *        }
     */
    fleshy: function(bonesFormattedThing, schema) {
      bonesFormattedThing["_fields"] = []
      bonesFormattedThing["_meta"] = []
      Object.keys(schema).forEach(function(key) {
        // Add to the displayFields array if not in the meta
        let pushInto = window.fleshConfig.metaFields.includes(key)
          ? "_meta"
          : "_fields"
        bonesFormattedThing[pushInto].push({
          key: key,
          val: bonesFormattedThing[key],
          type: schema[key].instance,
          nuffin: !bonesFormattedThing[key],
          validators: schema[key].validators
        })
      })
      return bonesFormattedThing
    }
  })
}
