;(function($) {
  APP.post("#/create/Thing/:id/", function(context) {
    var self = this
    // console.log({ boneyCreate: "context" }, context)
    // console.log({ boneyCreate: "params" }, self.params)
    $.ajax({
      url: `${window.fleshConfig.bones}/Thing/${self.params.id}/Thing/`,
      method: "POST",
      dataType: "json",
      data: self.params.toHash(),
      success: function(data) {
        context.redirect(`#/list/Thing/${self.params.id}/`)
      },
      error: function(jqXHR, textStatus, err) {
        // console.log({ boneyCreate: "jqXHR" }, jqXHR)
        // console.log({ boneyCreate: "textStatus" }, textStatus)
        console.log({ boneyCreate: "err" }, err)
      }
    })
    // this.trigger('update-list-count');
  })

  APP.post("#/patch/Thing/:id", function(context) {
    var self = this
    // console.log({ boneyPatch: "context" }, context)
    // console.log({ boneyPatch: "params" }, self.params)
    let formData = self.params.toHash()
    let thinner = Object.keys(context.schema)
    $.ajax({
      url: `${window.fleshConfig.bones}/Thing/${this.params._id}/`,
      method: "PATCH",
      dataType: "json",
      data: thinner,
      success: function(data) {
        context.redirect(`#/engage/Thing/${self.params.id}/`)
      },
      error: function(jqXHR, textStatus, err) {
        // console.log({ boneyPatch: "jqXHR" }, jqXHR)
        // console.log({ boneyPatch: "textStatus" }, textStatus)
        console.log({ boneyPatch: "err" }, err)
      }
    })
    // this.trigger('update-list-count');
  })
})(jQuery)
