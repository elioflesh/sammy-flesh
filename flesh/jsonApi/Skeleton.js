var Skeleton = function(app) {
  this.helpers({
    fleshy: function(bonesFormattedThing, schema) {
      bonesFormattedThing["_fields"] = []
      for (scheme in schema) {
        if (
          schema[scheme] != "_id" &&
          schema[scheme] != "__v" &&
          schema[scheme] != "_fields"
        ) {
          val = bonesFormattedThing[schema[scheme]]
          bonesFormattedThing["_fields"].push({
            key: schema[scheme],
            val: val,
            nuffin: !val
          })
        }
      }
      return bonesFormattedThing
    }
  })
}
