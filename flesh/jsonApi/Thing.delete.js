;(function($) {
  APP.get("#/delete/Thing/:id/", function(context) {
    var self = this
    // console.log({ boneyDelete: "context" }, context)
    // console.log({ boneyDelete: "params" }, self.params)
    $.ajax({
      url: `${window.fleshConfig.bones}/Thing/${self.params.id}/`,
      type: "DELETE",
      dataType: "json",
      success: function(data) {
        context.redirect(`#/list/Thing/${self.params.id}/`)
      }
    })
    // this.trigger('update-list-count');
  })
})(jQuery)
