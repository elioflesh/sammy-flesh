var FleshyPlugins = function(app) {
  this.helpers({
    slugify: function(text) {
      return text
        .toLowerCase()
        .trim()
        .replace(
          /[\u2000-\u206F\u2E00-\u2E7F\\'!"#$%&()*+,./:;<=>?@[\]^`{|}~]/g,
          ""
        )
        .replace(/\s/g, "-")
    }
  })
}
