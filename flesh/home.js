;(function($) {
  APP.get("#/home/about", function() {
    this.title = "About"
    this.slug = this.slugify(this.title)
    this.about = true
    this.partial(`${window.fleshConfig.templates}home.ms`)
  })

  APP.get("#/home/contact", function() {
    this.title = "Contact"
    this.slug = this.slugify(this.title)
    this.contact = true
    this.partial(`${window.fleshConfig.templates}home.ms`)
  })
})(jQuery)
