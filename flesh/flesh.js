;(function($) {
  APP = Sammy("#Flesh", function() {
    this.use("Mustache", "ms")
    this.use(FleshyPlugins)
    this.use(Skeleton)

    this.get("#/", function() {
      this.title = "Welcome back to the Flesh"
      this.slug = this.slugify(this.title)
      this.home = true
      this.partial(`${window.fleshConfig.templates}home.ms`)
    })
  })
  APP.run("#/")
})(jQuery)
